import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import {MenuModule} from 'primeng/menu';


import { AppComponent } from './app.component';
import { AccordionModule } from 'primeng/primeng';
import { ButtonModule } from 'primeng/button';
import { LoginComponent } from './components/login/login.component';
import { LoginModule } from './components/login/login.module';
import { AppRoutingModule } from './app-routing.module';
import { RouterModule } from '@angular/router';
import { TopBarComponent } from './components/top-bar/top-bar.component';
import { MenubarModule } from 'primeng/menubar';
import {InterceptorsModule } from './shared/interceptors/interceptors.module';
import { SideMenuComponent } from './components/side-menu/side-menu.component';
import { ContextMenuModule } from 'primeng/contextmenu';
import { PanelMenuModule } from 'primeng/panelmenu';
import {BrowserAnimationsModule} from '@angular/platform-browser/animations';
import {DashboardModule} from './components/dashboard/dashboard.module';
import { SettingsComponent } from './components/settings/settings.component';
import { CreateAccountComponent } from "./components/create-account/create-account.component";
import { CommonModule } from "@angular/common";
import { HttpClientModule } from "@angular/common/http";
import { InputTextModule } from "primeng/inputtext";
import { FormsModule } from "@angular/forms";
import { UserService } from "./data/user/services/user.service";
import { AuthGuard } from "./guard/auth.guard";


@NgModule({
  declarations: [
    AppComponent,
    TopBarComponent,
    SideMenuComponent,
    SettingsComponent,
    CreateAccountComponent
  ],
  imports: [
    CommonModule,
    BrowserModule,
    HttpClientModule,
    InterceptorsModule,
    AccordionModule,
    ButtonModule,
    InputTextModule,
    FormsModule,
    LoginModule,
    DashboardModule,
    AppRoutingModule,
    RouterModule,
    MenuModule,
    ContextMenuModule,
    PanelMenuModule,
    BrowserAnimationsModule,
    DashboardModule,
    MenubarModule
  ],
  providers: [UserService, AuthGuard ],
  bootstrap: [AppComponent],
  exports: [
    SideMenuComponent
  ]
})
export class AppModule { }
