import { Injectable } from '@angular/core';
import { ApiBaseService } from '../../../shared/base/services/apibaseservice.service';
import { Measurement} from '../classes/measurement.class';

@Injectable()
export class MeasurementService {

  constructor(private baseService: ApiBaseService) { }

  getMeasurementOfBike(serialNumber: string, amount: number = 0) {
    return this.baseService.get(Measurement, 'GetMeasurementsOfBike', {'SerialNumber': serialNumber, 'Amount': amount});
  }

  patchMeasurement(measurement: Measurement) {
    return this.baseService.patch(Measurement, '', measurement);
  }

  deleteMeasurement(measurement: string) {
    return this.baseService.delete(Measurement, measurement);
  }
}
