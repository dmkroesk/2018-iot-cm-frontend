import {IMeasurement} from './measurement.interface';
import {ApiControllable} from '../../../shared/base/classes/apicontrollable.class';

export class Measurement extends ApiControllable implements IMeasurement {
  public longitude: number;
  public latitude: number;
  public heading: number;
  public co2: number;
  public tvoc: number;
  public temperature: number;
  public pressure: number;
  public xAxis: number;
  public yAxis: number;
  public zAxis: number;

  public apiSubUrl = 'Measurement/';

  constructor(json?: Object) {
    super(json);
  }

  getDefaults(): any {
    return{
      longitude: -1,
      latitude: -1,
      heading: -1,
      co2: -1,
      tvoc: -1,
      temperature: -1,
      pressure: -1,
      xAxis: -1,
      yAxis: -1,
      zAxis: -1
    };
  }
}
