export interface IMeasurement {
  // GPS
  longitude: number;
  latitude: number;
  heading: number; // Might be String instead.

  // Air quality
  co2: number;
  tvoc: number;
  temperature: number;
  pressure: number;

  // Gyroscope
  xAxis: number;
  yAxis: number;
  zAxis: number;
}
