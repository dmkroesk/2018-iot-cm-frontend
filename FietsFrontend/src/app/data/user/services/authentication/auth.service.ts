import { Injectable, OnDestroy } from '@angular/core';
import 'rxjs/add/operator/do';
import 'rxjs/add/operator/shareReplay';
import 'rxjs/add/operator/takeUntil';
import 'rxjs/add/operator/filter';
import * as moment from 'moment';
import { User } from '../../classes/user.class';
import { ApiBaseService } from '../../../../shared/base/services/apibaseservice.service';

@Injectable()
export class AuthService implements OnDestroy {

  constructor(private baseService: ApiBaseService) {
  }

  /**
   * login with username and password
   * @param {string} username
   * @param {string} password
   * @returns {Observable<any>} object with jwt token and expires at date
   */
  login(username: string, password: string) {

    return this.baseService.post<any>(User, 'validate', {}, { username: username, password: password })
      .do(e => this.setSession(e))
      .shareReplay();
  }

  ngOnDestroy(): void {
  }

  /**
   * Adds jwt_token and expirasat date to local storage
   * @param authResult
   */
  private setSession(authResult) {
    if (authResult.token && authResult.expiresAt) {
      localStorage.setItem('jwt_token', authResult.token);
      localStorage.setItem('expires_at', JSON.stringify(authResult.expiresAt));
    }
  }

  /**
   * log out the user
   */
  logout() {
    localStorage.removeItem('jwt_token');
    localStorage.removeItem('expires_at');
  }

  /**
   * check if user is logged in
   * @returns {boolean} true if logged in
   */
  public isLoggedIn() {
    if (this.getExpiration() == null || localStorage.getItem('expires_at') == null) {
      return false;
    }

    return moment().isBefore(this.getExpiration());
  }

  /**
   * cehck if user is not logged in
   * @returns {boolean} true if not logged in
   */
  isLoggedOut() {
    return !this.isLoggedIn();
  }

  /**
   * get's the expiration date of the jwt token
   * @returns {moment.Moment}
   */
  getExpiration() {
    const expiration = localStorage.getItem('expires_at');
    const expiresAt = JSON.parse(expiration);
    return moment(expiresAt);
  }

}

