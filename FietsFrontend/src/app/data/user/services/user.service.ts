import { Injectable } from '@angular/core';
import {ApiBaseService} from '../../../shared/base/services/apibaseservice.service';
import {User} from '../classes/user.class';

@Injectable()
export class UserService {

  constructor(private baseService: ApiBaseService) { }

  /**
   * get user with username
   * @param {string} username
   * @returns {Observable<User>}
   */
  getUser(username: string) {
    return this.baseService.get(User, 'GetBike', {'userId': '1'});
  }

  /**
   * update user
   * @param {User} user
   * @returns {Observable<User>}
   */
  patchUser(user: User) {
    return this.baseService.patch(User, '', user);
  }

  /**
   * delete user
   * @param {string} username
   * @returns {Observable<User>}
   */
  deleteUser(username: string) {
    return this.baseService.delete(User, username);
  }

  /**
   * add user
   * @param {User} user
   * @returns {Observable<User>}
   */
  addUser(user: User) {
    return this.baseService.post(User, 'add', user);
  }

}
