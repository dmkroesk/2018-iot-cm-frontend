export interface IUser {
  firstName: string;
  lastName: string;
  serialNumber: string;
}
