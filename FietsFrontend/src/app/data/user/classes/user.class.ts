import {IUser} from './user.interface';
import {ApiControllable} from '../../../shared/base/classes/apicontrollable.class';

export class User  extends ApiControllable implements IUser {

  // sub url for  ApiBaseService
  public apiSubUrl = 'users/';

  constructor(json?: Object) {
    super(json);
  }

  public userName: string;
  public password: string;
  public firstName: string;
  public lastName: string;
  public serialNumber: string;

  /**
   * set's default values on initalisatiop
   * @returns {any}
   */
  getDefaults(): any {
    return {
      id: '',
      userName: '',
      password: '',
      firstName: '',
      lastName: '',
      serialNumber: ''
    };
  }
}
