import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { CreateAccountComponent } from "./components/create-account/create-account.component";
import { DashboardComponent } from "./components/dashboard/dashboard.component";
import { SettingsComponent } from "./components/settings/settings.component";
import { AuthGuard } from "./guard/auth.guard";

//add canActivate: [AuthGuard] for authentication to view the page
export const routes: Routes = [
  { path: 'registreren', component: CreateAccountComponent },
  { path: 'dashboard', component: DashboardComponent},
  { path: 'settings', component: SettingsComponent},
  { path: '', redirectTo: 'login', pathMatch: 'full' }
];

@NgModule({
  imports: [
    RouterModule.forRoot(routes)
  ],
  exports: [
    RouterModule
  ]
})
export class AppRoutingModule { }
