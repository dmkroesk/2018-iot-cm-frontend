import { OnDestroy } from '@angular/core';
import {ISubscription} from 'rxjs/Subscription';

/**
 * class to handle subscruption
 * override component with this class when {observable}.subscribe is used
 */
export class SubscribeHandler implements OnDestroy {

  public subscriptions: Array<ISubscription> = new Array<ISubscription>();

  /**
   * add subscription to subscriptions
   * automatically unsubscribed on NgOnDestroy
   * @param {ISubscription} the subscription
   * @example this.newSubscription = Observable.subscribe((value) => {})
   */
  public set newSubscription(subscription: ISubscription) {
    this.subscriptions.push(subscription);
  }

  ngOnDestroy(): void {
    this.subscriptions.forEach((subscription: ISubscription) => {
      subscription.unsubscribe();
    });
  }

}
