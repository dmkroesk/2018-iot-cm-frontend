import {IApiControllable} from '../interfaces/iapicontrollable.interface';

export abstract class ApiControllable implements  IApiControllable {

  // id of the class
  public id: any;
  // sub url for ApiBaseService
  public abstract apiSubUrl: string;

  /**
   * Define defaults
   */
  public abstract getDefaults(): any;

  /**
   * constructor
   * @param {Object} json for initialization
   */
  constructor(json?: Object) {
      this.initDefaults();
      this.loadFromJson(json);
  }

  /**
   * set's properties from json to object
   * @param {Object} json
   */
  public loadFromJson(json?: Object) {
    for (const prop in json) {
      if (!json.hasOwnProperty(prop)) {
        continue;
      }

      if (json[prop] instanceof Object) {
        this[prop] = JSON.parse(json[prop]);
      } else {
        this[prop] = json[prop];
      }
    }
  }

  /**
   * Initialize defaults.
   */
  public initDefaults() {
    const defaults: any = this.getDefaults();

    for (const prop in defaults) {
      if (Array.isArray(defaults[prop])) {
        this[prop] = new Array<any>();
      } else {
        this[prop] = defaults[prop];
      }
    }
  }
}
