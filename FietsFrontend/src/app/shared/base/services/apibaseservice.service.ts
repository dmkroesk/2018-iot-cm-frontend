import { Injectable } from '@angular/core';
import {ApiControllable} from '../classes/apicontrollable.class';
import {Observable} from 'rxjs/Observable';
import {HttpClient, HttpParams} from '@angular/common/http';
import {INewable} from '../interfaces/inewable.interface';

@Injectable()
export class ApiBaseService {

  constructor(private http: HttpClient) { }

  /**
   * gets api controllable from server
   * @param {INewable<T>} type Type requesting
   * @param {string} request suburl for get request
   * @param {Object} params parameters
   * @returns {Observable<T>} the result
   * @example get(User, 'add', { id: '1'})
   */
  public get<T>(type: INewable<T>, request: string, params?: Object): Observable<T> {
    const dataType = new type();
    if (dataType instanceof  ApiControllable) {
      return this.http.get<T>(dataType.apiSubUrl + request, { params: this.toHttpParams(params)});
    }
  }

  /**
   * deletes apicontrollable from server
   * @param {INewable<T>} type
   * @param {string} request
   * @param {Object} params
   * @returns {Observable<T>}
   * @example delete(User, user.username)
   */
  public delete<T>(type: INewable<T>, request: string, params?: Object): Observable<T> {
    const dataType = new type();
    if (dataType instanceof  ApiControllable) {
      return this.http.delete<T>(dataType.apiSubUrl + request, { params: this.toHttpParams(params)});
    }
  }

  /**
   * update apicontrollable on server
   * @param {INewable<T>} type
   * @param {string} request
   * @param body
   * @param {Object} params
   * @returns {Observable<T>}
   * @example patch(User, '', this.user)
   */
  public patch<T>(type: INewable<T>, request: string, body: any, params?: Object): Observable<T> {
    const dataType = new type();
    if (dataType instanceof  ApiControllable) {
      return this.http.patch<T>(dataType.apiSubUrl + request, body, { params: this.toHttpParams(params)});
    }
  }

  /**
   * post apicontrollable to server
   * @param {INewable<T>} type
   * @param {string} request
   * @param body
   * @param {Object} params
   * @returns {Observable<T>}
   * @example post(User, 'validate', {username: 'cas', password: '@Password1'})
   */
  public post<T>(type: INewable<T>, request: string, body: any, params?: Object): Observable<T> {
    const dataType = new type();
    if (dataType instanceof  ApiControllable) {
      return this.http.post<T>(dataType.apiSubUrl + request, body, { params: this.toHttpParams(params)});
    }
  }

  private toHttpParams(params) {
    return Object.getOwnPropertyNames(params)
      .reduce((p, key) => p.set(key, params[key]), new HttpParams());
  }
}
