export interface IApiControllable {

  id: any;
  apiSubUrl: string;

  loadFromJson(json: Object);
  initDefaults();
}
