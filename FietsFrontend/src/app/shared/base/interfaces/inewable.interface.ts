/**
 * interface to initiate class
 */
export interface INewable<T> {
  new (): T;
  new (data: Object): T;
}
