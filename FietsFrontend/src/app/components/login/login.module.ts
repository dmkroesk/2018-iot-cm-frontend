import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { LoginRoutingModule } from './login-routing.module';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { HttpClientModule } from '@angular/common/http';
import { AuthService } from '../../data/user/services/authentication/auth.service';
import { ApiBaseService } from '../../shared/base/services/apibaseservice.service';

import { LoginComponent } from './login.component';

import { ButtonModule } from 'primeng/button';
import { InputTextModule } from 'primeng/inputtext';

@NgModule({
  imports: [
    CommonModule,
    LoginRoutingModule,
    ReactiveFormsModule,
    HttpClientModule,
    FormsModule,
    ButtonModule,
    InputTextModule

  ],
  declarations: [LoginComponent],
  providers: [AuthService, ApiBaseService]
})
export class LoginModule { }
