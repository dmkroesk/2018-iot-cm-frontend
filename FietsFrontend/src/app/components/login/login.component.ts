import { Component, OnInit } from '@angular/core';
import { AuthService } from '../../data/user/services/authentication/auth.service';
import { SubscribeHandler } from '../../shared/base/classes/subscribehandler.class';
import { Router } from "@angular/router";

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.scss']
})
export class LoginComponent extends SubscribeHandler implements OnInit {

  username: string;
  password: string;

  constructor(private auth: AuthService, private router: Router) {
    super();
  }


  ngOnInit() {
    if (this.auth.isLoggedIn()) {
      this.router.navigate(['dashboard']);
    }

  }

  login() {
    this.newSubscription = this.auth.login(this.username, this.password).subscribe((response) => {
      console.log(response);
        if (this.auth.isLoggedIn()) {
      this.router.navigate(['dashboard']);
    }
    });

  


  }

}
