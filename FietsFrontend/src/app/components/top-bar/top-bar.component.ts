import { Component, OnInit, ViewEncapsulation } from '@angular/core';
import { MenuItem } from 'primeng/api';
import { AuthService } from "../../data/user/services/authentication/auth.service";
import { Observable } from "rxjs/Observable";


@Component({
  selector: 'app-top-bar',
  templateUrl: './top-bar.component.html',
  styleUrls: ['./top-bar.component.scss'],
  moduleId: module.id,
  encapsulation: ViewEncapsulation.None
})
export class TopBarComponent implements OnInit {

  items: MenuItem[];
  isLoggedIn$: Observable<boolean>;

  constructor(private auth: AuthService) { }

  ngOnInit() {
    this.initMenuBar();
    console.log('komt hierrooo');
  //  this.isLoggedIn$ = this.auth.isLoggedIn;
  }

  initMenuBar() {
    this.items = [
      {
        label: 'Acount',
        icon: 'fa-user',
        items: [
          {label: 'Profiel', icon: 'fa-user'},
          {label: 'Help', icon: 'fa-question-circle'},
          {label: 'Instellingen', icon: 'fa-cogs'},
          {label: 'Log uit', icon: 'fa-sign-out'},
        ]
      }
    ];
  }

}
