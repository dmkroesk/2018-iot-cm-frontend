import { Component, OnInit } from '@angular/core';
import { User } from "../../data/user/classes/user.class";
import { SubscribeHandler } from "../../shared/base/classes/subscribehandler.class";
import { UserService } from "../../data/user/services/user.service";

@Component({
  selector: 'app-create-account',
  templateUrl: './create-account.component.html',
  styleUrls: ['./create-account.component.scss']
})
export class CreateAccountComponent extends SubscribeHandler implements OnInit {

  private user: User;

  constructor(private auth: UserService) {
    super();
  }

  ngOnInit() {
    this.user = new User();
  }

  register() {
    console.log(this.user);
    this.newSubscription = this.auth.addUser(this.user).subscribe((response) => {
      console.log(response);
    });
  }

}
