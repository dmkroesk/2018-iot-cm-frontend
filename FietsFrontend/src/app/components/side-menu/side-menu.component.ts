import {Component, OnInit, ViewEncapsulation} from '@angular/core';
import { MenuItem } from 'primeng/api';

@Component({
  selector: 'app-side-menu',
  templateUrl: './side-menu.component.html',
  styleUrls: ['./side-menu.component.scss'],
  moduleId: module.id,
  encapsulation: ViewEncapsulation.None
})
export class SideMenuComponent implements OnInit {

  /**
   * Holds the menu items for rendering.
   * @type {Array<MenuItem>}
   */
  public menuItems: Array<MenuItem>;

  constructor() { }

  ngOnInit() {
    this.menuItems = [
      {label: 'Dashboard', icon: 'fa-home', routerLink: 'dashboard'},
      {label: 'Fietscomputer', icon: 'fa-tachometer'},
      {label: 'Instellingen', icon: 'fa-cogs', routerLink: 'settings'}
    ];
  }
}
