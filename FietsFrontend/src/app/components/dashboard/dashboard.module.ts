import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import {DashboardComponent} from './dashboard.component';
import {WidgetComponent} from './widget/widget.component';
import {GridsterModule} from 'angular-gridster2';

@NgModule({
  imports: [
    CommonModule,
    GridsterModule
  ],
  declarations: [DashboardComponent, WidgetComponent],
  exports: [DashboardComponent, WidgetComponent]
})
export class DashboardModule { }
